<?php

return [
    'url' => env('API_CLIENT_URL', 'http://localhost/api'),
    'login' => env('API_CLIENT_LOGIN', 'user'),
    'password' => env('API_CLIENT_PASSWORD', 'secret'),
];
