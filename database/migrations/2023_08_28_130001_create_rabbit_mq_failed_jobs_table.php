<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('rabbit_mq_failed_jobs', function (Blueprint $table) {
            $table->id()->comment('Уникальный идентификатор записи');
            $table->text('queue')->comment('Название очереди');
            $table->longText('payload')->comment('Полезная нагрузка');
            $table->longText('exception')->comment('Исключение');
            $table->timestamp('failed_at')->useCurrent()->comment('Дата и время возникновения исключения');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('rabbit_mq_failed_jobs');
    }
};
