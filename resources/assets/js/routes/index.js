import * as VueRouter from 'vue-router';

const router = VueRouter.createRouter({
  history: VueRouter.createWebHistory(),
  routes: [
    {
      path: '',
      component: () => import('../views/layout/FeedbackForm.vue'),
    },
  ]
});

router.beforeEach(async (to, from, next) => {
  next();
});

export default router;
