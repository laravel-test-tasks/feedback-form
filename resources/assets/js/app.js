import axios from "axios";
import Lodash from "lodash";

import {createApp} from 'vue';
import ElementPlus from 'element-plus';
import ElementRuLocale from 'element-plus/dist/locale/ru.min.mjs';
import AppNotify from './plugins/appNotify.js';

import App from './components/App.vue';
import router from "./routes";

import './bootstrap.js';

import 'element-plus/dist/index.css';
import "element-plus/theme-chalk/src/message.scss";

const app = createApp(App);

app
  .use(ElementPlus, {
    locale: ElementRuLocale
  })
  .use(router)
  .use(AppNotify);

app.mount('#app');

window.axios = axios;
window._ = Lodash;
