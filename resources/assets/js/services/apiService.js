export default {
  get(path, params = {}) {
    return new Promise((resolve, reject) => {
      window.axios.get(path, {params})
        .then(function (response) {
          resolve(response);
        })
        .catch(function (error) {
          reject(error);
        });
    });
  },
  post(path, data) {
    return new Promise((resolve, reject) => {
      window.axios.post(path, data)
        .then(function (response) {
          resolve(response);
        })
        .catch(function (error) {
          reject(error);
        });
    });
  },
  update(path, data) {
    return new Promise((resolve, reject) => {
      window.axios.post(path, {...data, ...{_method: 'PUT'}})
        .then(function (response) {
          resolve(response);
        })
        .catch(function (error) {
          reject(error);
        });
    });
  },
  delete(path) {
    return new Promise((resolve, reject) => {
      window.axios.delete(path)
        .then(function (response) {
          resolve(response);
        })
        .catch(function (error) {
          reject(error);
        });
    });
  },
  download(path, params = {}) {
    return new Promise((resolve, reject) => {
      window.axios({
        url: path,
        method: 'GET',
        responseType: 'blob',
        params,
      }).then((response) => {
        const url = window.URL.createObjectURL(new Blob([response.data]));
        const link = document.createElement('a');
        link.href = url;
        link.setAttribute('download', response.headers['content-disposition'].split('filename=')[1]);
        document.body.appendChild(link);
        link.click();

        resolve(response);
      }).catch((error) => {
        reject(error);
      });
    });
  },
}
