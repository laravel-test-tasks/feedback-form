import {ElNotification} from "element-plus";

export default {
  install(app) {
    app.config.globalProperties.$appNotify = {
      success: function (value, options = {}) {
        if (typeof value === 'string') {
          value = [value];
        }

        if (!Array.isArray(value)) {
          console.error('Notify: Bad content');
          return false;
        }

        value.forEach(function (text) {
          if (typeof text !== 'string') {
            console.error('Notify: Bad content');
            return false;
          }

          _.defer(function () {
            ElNotification.success({
              title: 'Успех',
              message: text,
              ...options
            });
          });
        });

        return true;
      },
      error: function (value, options = {}) {
        if (typeof value === 'string') {
          value = [value];
        } else if (typeof value === 'object') {
          let list = [];

          for (let key in value.errors) {
            list.push(value.errors[key][0]);
          }

          if (list.length <= 0 && value.message) {
            list.push(value.message);
          }

          value = list;
        }

        value.forEach(function (text) {
          if (typeof text !== 'string') {
            console.error('Notify: Bad content');
            return false;
          }

          if (text) {
            _.defer(function () {
              ElNotification.error({
                title: 'Ошибка',
                message: text,
                ...options,
              });
            });
          }
        });

        return true;
      }
    }
  }
}
