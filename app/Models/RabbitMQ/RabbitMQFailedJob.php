<?php

namespace App\Models\RabbitMQ;

use Carbon\Carbon;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\RabbitMQ\RabbitMQFailedJob
 *
 * @property int $id Уникальный идентификатор записи
 * @property string $queue Название очереди
 * @property string $payload Полезная нагрузка
 * @property string $exception Исключение
 * @property string $failed_at Дата и время возникновения исключения
 * @method static Builder|RabbitMQFailedJob newModelQuery()
 * @method static Builder|RabbitMQFailedJob newQuery()
 * @method static Builder|RabbitMQFailedJob query()
 * @method static Builder|RabbitMQFailedJob whereException($value)
 * @method static Builder|RabbitMQFailedJob whereFailedAt($value)
 * @method static Builder|RabbitMQFailedJob whereId($value)
 * @method static Builder|RabbitMQFailedJob wherePayload($value)
 * @method static Builder|RabbitMQFailedJob whereQueue($value)
 * @mixin Eloquent
 */
class RabbitMQFailedJob extends Model
{
    use HasFactory;

    /** @var string $table */
    protected $table = 'rabbit_mq_failed_jobs';

    /** @var bool $timestamps */
    public $timestamps = false;

    /** @var string[] $fillable */
    protected $fillable = [
        'queue',
        'payload',
        'exception',
    ];

    /**
     * @return void
     */
    public function setFailedAtAttribute(): void
    {
        $this->attributes['failed_at'] = Carbon::now();
    }
}
