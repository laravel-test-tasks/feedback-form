<?php

namespace App\Models\Feedback;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Feedback\FeedbackMessage
 *
 * @property int $id
 * @property string $email
 * @property string $phone
 * @property string $comment
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|FeedbackMessage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FeedbackMessage newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FeedbackMessage query()
 * @method static \Illuminate\Database\Eloquent\Builder|FeedbackMessage whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FeedbackMessage whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FeedbackMessage whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FeedbackMessage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FeedbackMessage wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FeedbackMessage whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class FeedbackMessage extends Model
{
    /** @var string[] $fillable */
    protected $fillable = [
        'comment',
        'email',
    ];
}
