<?php

namespace App\Jobs\Feedback;

use App\Dto\Feedback\FeedbackMessage;
use App\Jobs\RabbitMQJob;
use Bunny\Channel;
use Bunny\Client;
use Bunny\Message;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Throwable;

class FeedbackMessageProcessJob extends RabbitMQJob
{
    protected static string $queue = 'feedback-message';

    /**
     * @param string|array $message
     * @param array $headers
     * @param string $exchangeName
     * @return void
     */
    protected function handlePublish(&$message, array &$headers, string &$exchangeName): void {
        $message = json_encode($message);
    }

    protected bool $anywayMessageAsRead = true;

    /**
     * @param Message $message
     * @param Channel $channel
     * @param Client $client
     * @throws Throwable
     */
    protected function handleConsume(Message $message, Channel $channel, Client $client): void
    {
        sleep(5); // for the task. Imitation

        $feedbackMessage = FeedbackMessage::init($message->content);

        Log::channel('feedback')->info([
            "Время: " . Carbon::parse($feedbackMessage->eventAt),
            "URL: " . config('apiclient.url'),
            "Логин: " . config('apiclient.login'),
            "Пароль: " . config('apiclient.password'),
            "Email: $feedbackMessage->email",
            "Комментарий: $feedbackMessage->comment",
        ]);
    }
}
