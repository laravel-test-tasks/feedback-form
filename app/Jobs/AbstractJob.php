<?php

namespace App\Jobs;

use Throwable;
use Log;

abstract class AbstractJob
{
    /**
     * @return void
     */
    public function handle(): void
    {
        try {
            $this->customHandle();
        } catch (Throwable $e) {
            $this->failed($e);
        }
    }

    protected abstract function customHandle();

    /**
     * @param Throwable $e
     * @return void
     */
    public function failed(Throwable $e): void
    {
        Log::error($e->getMessage(), $e->getTrace());
    }
}
