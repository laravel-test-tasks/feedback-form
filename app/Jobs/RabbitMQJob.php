<?php

namespace App\Jobs;

use App\Models\RabbitMQ\RabbitMQFailedJob;
use App\Services\RabbitMQ\RabbitMQClient;
use Bunny\Channel;
use Bunny\Client;
use Bunny\Message;
use Illuminate\Support\Facades\Log;
use React\Promise\PromiseInterface;
use Throwable;

abstract class RabbitMQJob
{
    /** @var string $queue */
    protected static string $queue;

    /** @var Channel|mixed|PromiseInterface $channel */
    protected $channel;

    /** @var RabbitMQClient|Client $client */
    protected $client;

    /**
     * Любом случаи пометить сообщение как прочитанное
     * @var bool
     */
    protected bool $anywayMessageAsRead = false;

    /**
     * @param Client|null $client
     * @param $channel
     */
    public function __construct(Client $client = null, $channel = null)
    {
        $this->client = $client ?? (new RabbitMQClient())->getClient();

        $this->channel = $channel ?? $this->client->channel();
    }

    /**
     * @return string
     */
    public static function getQueue(): string
    {
        return static::$queue;
    }

    /**
     * Обработчик сообщения, который отправляется в очередь
     *
     * @param string|array $message
     * @param array $headers
     * @param string $exchangeName
     * @return void
     */
    abstract protected function handlePublish(&$message, array &$headers, string &$exchangeName): void;

    /**
     * Обработчик полученного сообщения от очереди.
     *
     * @param Message $message Полученное сообщение
     * @param Channel $channel Канал
     * @param Client $client Клиент
     * @return void
     */
    abstract protected function handleConsume(Message $message, Channel $channel, Client $client): void;

    /**
     * Публикация сообщения в очередь
     *
     * @param string|array $message Сообщение
     * @param array $headers Дополнительные параметры (заголовки)
     * @param string $exchangeName Название переключателя
     * @return $this
     */
    public function publish($message, array $headers = [], string $exchangeName = ''): RabbitMQJob
    {
        $this->handlePublish($message, $headers, $exchangeName);

        $this->channel->publish(
            $message,
            $headers,
            $exchangeName,
            static::$queue
        );

        return $this;
    }

    /**
     * Запуск consumer
     *
     * @return $this
     */
    public function consume(): RabbitMQJob
    {
        $this->channel->consume(
            function (Message $message, Channel $channel, Client $client) {
                try {
                    $this->handleConsume($message, $channel, $client);

                    $channel->ack($message);
                } catch (Throwable $e) {
                    if ($this->anywayMessageAsRead) {
                        $channel->ack($message);
                    }

                    Log::channel('rabbitmq')->error($e);

                    RabbitMQFailedJob::create([
                        'queue' => $message->routingKey,
                        'payload' => $message->content,
                        'exception' => $e,
                    ]);
                }
            },
            static::$queue
        );

        return $this;
    }

    /**
     * Получить channel
     *
     * @return Channel|mixed|PromiseInterface
     */
    public function getChannel()
    {
        return $this->channel;
    }

    /**
     * Получить client
     *
     * @return RabbitMQClient|Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Запуск клиента
     *
     * @param int|null $maxSeconds
     * @return $this
     */
    public function runClient(int $maxSeconds = null): RabbitMQJob
    {
        $this->client->run($maxSeconds);

        return $this;
    }
}
