<?php

namespace App\Dto\Feedback;

use App\Dto\DeserializedApiItem;

class FeedbackMessage extends DeserializedApiItem
{
    /** @var string $email */
    public string $email;

    /** @var string $comment */
    public string $comment;

    /** @var string $eventAt Дата и время события */
    public string $eventAt;
}
