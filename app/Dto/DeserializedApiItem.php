<?php
/**
 * Класс для десериализации строки json в объект php.
 * Требуется создать наследника, в рамках которого будут описаны необходимые свойства объекта.
 */

namespace App\Dto;

use Symfony\Component\PropertyInfo\Extractor\PhpDocExtractor;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\PropertyInfo\PropertyInfoExtractor;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

abstract class DeserializedApiItem
{
    /**
     * @param string|array $data - string|array with xml or json data
     * @return mixed
     */
    public static function init($data = '')
    {
        $data = is_array($data) ? json_encode($data) : (!$data ? '[]' : $data);

        $reflectionExtractor = new ReflectionExtractor();

        $phpDocExtractor = new PhpDocExtractor();

        $propertyTypeExtractor = new PropertyInfoExtractor(
            [$reflectionExtractor],
            [$phpDocExtractor, $reflectionExtractor],
            [$phpDocExtractor],
            [$reflectionExtractor],
            [$reflectionExtractor]
        );

        $normalizer = new ObjectNormalizer(
            null, null, null, $propertyTypeExtractor
        );

        $arrayNormalizer = new ArrayDenormalizer();

        $serializer = new Serializer([$arrayNormalizer, $normalizer], [new JsonEncoder()]);

        return $serializer->deserialize($data, get_called_class(), 'json');
    }

    /**
     * @param int $options
     * @return false|string
     */
    public function toJson(int $options = 0)
    {
        return json_encode($this, $options);
    }

    private function __construct() {}
}
