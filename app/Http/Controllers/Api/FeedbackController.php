<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Feedback\FeedbackStoreRequest;
use App\Services\Feedback\FeedbackMessageSaver;
use App\Services\Feedback\FeedbackMessageService;
use DB;
use Illuminate\Database\Eloquent\Model;
use Throwable;

class FeedbackController extends Controller
{
    /**
     * @param FeedbackStoreRequest $request
     * @return Model
     * @throws Throwable
     */
    public function store(FeedbackStoreRequest $request): Model
    {
        DB::beginTransaction();

        $feedbackMessageSaver = new FeedbackMessageSaver();
        $feedbackMessageSaver
            ->fill($request->all())
            ->save();

        FeedbackMessageService::sendQueueFeedbackMessage($feedbackMessageSaver->getInstance());

        DB::commit();

        return $feedbackMessageSaver->getInstance();
    }
}
