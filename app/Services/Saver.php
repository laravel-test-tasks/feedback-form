<?php

namespace App\Services;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Services\Saver
 *
 * @property Model $instance
 * @property boolean $saved
 */
abstract class Saver
{
    protected $instance;
    protected $saved = false;

    abstract function __construct($instance = null);

    /*
    Пример, того что вы должны сделать, наследуясь от Saver
    function __construct($instance = null)
    {
        $this->instance = $instance ?? new Form();
    }
    */

    function getInstance()
    {
        return $this->instance;
    }

    function fill($data)
    {
        $this->instance->fill($data);

        return $this;
    }

    function save()
    {
        $this->saved = $this->instance->saveOrFail();

        return $this;
    }

    final function syncRelations(array $data, $instance, $relationName, $class)
    {
        $data = new Collection($data);

        $this->deleteRelations($data, $instance->$relationName);

        foreach ($data as $datum) {
            $this->updateOrCreateRelation($datum, $instance, $relationName, $class);
        }
    }

    final function deleteRelations(Collection $data, $relation): void
    {
        /**
         * @var Model $model
         * @var Collection $relation
         */
        foreach ($relation as $i => $model) {
            if (!$data->contains('id', $model->id)) {
                $model->delete();

                $relation->forget($i);
            }
        }
    }

    final function updateOrCreateRelation(array $datum, $instance, $relationName, $class)
    {
        /**
         * @var Model $model
         * @var Collection $relation
         */
        if (data_get($datum, 'id', 0) > 0) {
            $model = $instance->$relationName
                ->where('id', $datum['id'])
                ->shift();

            $model->update($datum);
        } else {
            $model = new $class($datum);

            $instance->$relationName()
                ->save($model);

            $instance->$relationName->add($model);
        }

        return $model;
    }

    final function when($value, $callback, $default = null)
    {
        if ($value) {
            return $callback($this, $value) ?: $this;
        } elseif ($default) {
            return $default($this, $value) ?: $this;
        }

        return $this;
    }
}
