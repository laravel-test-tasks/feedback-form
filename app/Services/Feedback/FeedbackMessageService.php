<?php

namespace App\Services\Feedback;

use App\Jobs\Feedback\FeedbackMessageProcessJob;
use App\Models\RabbitMQ\RabbitMQFailedJob;
use Illuminate\Support\Facades\Log;
use Throwable;

class FeedbackMessageService
{
    /**
     * @param $feedbackMessage
     * @return void
     */
    public static function sendQueueFeedbackMessage($feedbackMessage): void
    {
        $queueMessage = [
            'email' => $feedbackMessage->email,
            'comment' => $feedbackMessage->comment,
            'eventAt' => $feedbackMessage->created_at,
        ];

        try {
            $feedbackMessageProcessJob = new FeedbackMessageProcessJob();
            $feedbackMessageProcessJob->publish($queueMessage);
        } catch (Throwable $e) {
            Log::channel('rabbitmq')->error($e);

            RabbitMQFailedJob::create([
                'queue' => FeedbackMessageProcessJob::getQueue(),
                'payload' => json_encode($queueMessage),
                'exception' => $e,
            ]);

            abort(422, __('messages.errors.rabbitmq.publish_feedback_message'));
        }
    }
}
