<?php

namespace App\Services\Feedback;

use App\Models\Feedback\FeedbackMessage;
use App\Services\Saver;

class FeedbackMessageSaver extends Saver
{
    function __construct($instance = null)
    {
        $this->instance = $instance ?? new FeedbackMessage();
    }
}
