<?php

namespace App\Services\RabbitMQ;

use Bunny\Client;
use Exception;

class RabbitMQClient
{
    /** @var Client $client */
    private Client $client;

    /**
     * @throws Exception
     */
    public function __construct()
    {
        $connection = [
            'host' => config('queue.connections.rabbitmq.hosts.host'),
            'port' => config('queue.connections.rabbitmq.hosts.port'),
            'vhost' => config('queue.connections.rabbitmq.hosts.vhost'),
            'user' => config('queue.connections.rabbitmq.hosts.user'),
            'password' => config('queue.connections.rabbitmq.hosts.password'),
        ];

        $this->client = new Client($connection);
        $this->client->connect();

        if (!$this->client->isConnected()) {
            throw new Exception("Cannot connect to the RabbitMQ broker!");
        }

        return $this->client;
    }

    /**
     * @return Client
     */
    public function getClient(): Client
    {
        return $this->client;
    }
}
