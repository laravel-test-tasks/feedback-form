<?php

namespace App\Console\Commands;

use App\Jobs\Feedback\FeedbackMessageProcessJob;
use Illuminate\Console\Command;
use App\Jobs\RabbitMQJob;
use App\Services\RabbitMQ\RabbitMQClient;

class WorkRabbitMQCommand extends Command
{
    private array $jobs = [
        FeedbackMessageProcessJob::class,
    ];

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rabbitmq:work';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Running RabbitMQ job';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {
        $this->info('RabbitMQ:connect...');

        $rabbitMQClient = (new RabbitMQClient())->getClient();

        $this->info('RabbitMQ:connected');

        /** @var RabbitMQJob|null $processJob */
        $processJob = null;

        $this->info('RabbitMQ:registration-jobs');

        /** @var RabbitMQJob $job */
        foreach ($this->jobs as $job) {
            if (is_null($processJob)) {
                $processJob = new $job($rabbitMQClient, $rabbitMQClient->channel());
            } else {
                $processJob = new $job($processJob->getClient(), $processJob->getChannel());
            }

            $processJob->consume();

            $this->info($job  . ": registered");
        }

        if ($processJob === null) {
            $this->warn('RabbitMQ:jobs-not-found');

            return 1;
        }

        $this->info('RabbitMQ:running-client');

        $processJob->runClient();
    }
}
