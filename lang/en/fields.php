<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Field Language Resources
    |--------------------------------------------------------------------------
    */

    // Feedback form
    'feedback.email' => 'Email',
    'feedback.comment' => 'Comment',
];
