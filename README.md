## Установка компонентов

В зависимости от OS нужно установить следующие компоненты:

- [Git](https://git-scm.com/)
- [Composer v.2.*](https://getcomposer.org/)
- [Node JS v.18.16.0](https://nodejs.org/en/)
- [PHP v8.3 (linux)](https://losst.ru/ustanovka-php-7-v-ubuntu-2)
- [PostgreSQL v.15](https://www.postgresql.org/)
- [PgAdmin (если нужен)](https://www.pgadmin.org/)
- [OpenServer (Windows, php v8.3)](https://biblprog.org.ua/ru/open_server/)

## Установка проекта

- Склонировать проект:

  <code>git clone https://gitlab.com/laravel-test-tasks/feedback-form.git</code>

- Перейти к проекту:

  <code>cd ваша директория</code>

- Переключиться на ветку **dev**:

  <code>git checkout dev</code>

- Установить и обновить пакеты Laravel через composer:

  <code>composer install && composer update</code>

- Установить и собрать пакеты node_modules:

  <code>npm install && npm run build</code>

- Создать .env файл, копируя с .env.example:

  <code>cp .env.example .env</code>

- Сгенерировать key проекта:

  <code>php artisan key:generate</code>

- Создать базу данных, которую будете указывать в .env-файле для параметра DB_DATABASE.

- Установить rabbitmq
